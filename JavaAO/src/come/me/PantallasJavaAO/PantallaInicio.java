package come.me.PantallasJavaAO;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.me.NucleoJavaAO.NucleoJavaAO;

public class PantallaInicio extends PantallaAbs {

	private BitmapFont fontFPS;
	private SpriteBatch sp;
	
	private Skin skin;
	private Stage stage;
	
	private Table tabla;
	private Button botonLogear;
	private TextField txtNombre;
	
	private NucleoJavaAO juego;
	
	public PantallaInicio(final NucleoJavaAO juego) {   
		super(juego);
		this.juego = juego;

    	sp    = new SpriteBatch();
        skin  = new Skin(Gdx.files.internal("skin/uiskin.json"));
        stage = new Stage();
 
        tabla = new Table();
        tabla.setFillParent(true);
        botonLogear = new TextButton("Logear", skin, "default");
        txtNombre   = new TextField("", skin);
       
        tabla.add(new Label("Nombre : ", skin));
        tabla.add(txtNombre);
        tabla.row();
        tabla.add(botonLogear).colspan(2);

        stage.addActor(tabla);
        
        botonLogear.addListener(new ClickListener() {
        	@Override
        	public void clicked(InputEvent event, float x, float y) {
        		juego.getPersonajePrueba().setNombre( txtNombre.getText() );
        		juego.setScreen( new PantallaPrincipal(juego) );
        		dispose();
        	}
		});
 
        juego.getMulti().addProcessor(stage);
        //Gdx.input.setInputProcessor(stage);
	}

    @Override
    public void dispose() {
        sp.dispose();
        stage.dispose();
    }

	@Override
	public void render(float delta) {   
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
    	stage.getViewport().setScreenSize(width, height);
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

}
