package come.me.PantallasJavaAO;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.graphics.PixmapIO.PNG;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.me.ModeloJavaAO.IndividuoAbs;
import com.me.NucleoJavaAO.Constantes;
import com.me.NucleoJavaAO.NucleoJavaAO;
import com.me.NucleoJavaAO.RecursosJuego;

/**
 * Clase encargada de dibujar en pantalla el juego.
 * @author spirok
 * 02/06/15
 */
public class PantallaPrincipal extends PantallaAbs {

	private BitmapFont fontFPS;
	private IndividuoAbs pruebaPJ;
	private OrthographicCamera camara;
	private SpriteBatch batchMapa;
	private OrthogonalTiledMapRenderer renderMapa;
	
	private Skin skin;
	private Stage stage;
	private TextButton boton;
	
    int[] layersFondo = { 0, 1, 2};
    int[] layersFrente = {3 };
  
    private TextureRegion txtArbol;
    
    
	public PantallaPrincipal(NucleoJavaAO juego) {
		super(juego);
	}

	@Override
	public void show() {
		fontFPS    = new BitmapFont();
		camara     = juego.getCamara();
		batchMapa  = juego.getSBMapa();
		renderMapa = juego.getRenderMapa();
		pruebaPJ   = juego.getPersonajePrueba();

		stage = new Stage();
		skin  = new Skin(Gdx.files.internal("skin/uiskin.json"));
		boton = new TextButton("Deslogear", skin);
		
		boton.setPosition(Constantes.ANCHOPANTALLA - 100, 0);
		
		stage.addActor(boton);

		boton.addListener(new ClickListener() {
        	@Override
        	public void clicked(InputEvent event, float x, float y) {
        		juego.setScreen( new PantallaInicio(juego) );
        		dispose();
        	}
		});
		
		juego.getMulti().addProcessor(new InputAdapter() {	
			@Override
			public boolean keyTyped(char character) {
				//System.out.println(character);
				if (character == 'z') juego.getCamara().zoom += 0.2f;
				if (character == 'a') juego.getCamara().zoom -= 0.2f;
				return false;
			}
		});
		
        juego.getMulti().addProcessor(stage);
        
        txtArbol = RecursosJuego.acomodarImagen(Gdx.files.local(Constantes.rutaImagenes + "1170.png"));
        
	}
	
	/*
	 * 
	 */
	@Override
	public void render(float delta) {
		// actualizando estado del personaje
		pruebaPJ.actualizar(delta);
		
		// limpiando pantalla
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        
        // actualizado camara
        camara.update();
        
        // seteando vista del renderMapa con la camara
        renderMapa.setView(camara);
        

        // dibujando los layer de fondo
        renderMapa.render( layersFondo );
        
        //renderMapa.render();
        batchMapa.begin();
        // dibujando personaje
        pruebaPJ.dibujar(batchMapa);
        // dibujando los fps
        fontFPS.draw(batchMapa, "FPS: " + Gdx.graphics.getFramesPerSecond(), 
        		(camara.position.x - (Gdx.graphics.getWidth() / 2)) + 10,
        		(camara.position.y - (Gdx.graphics.getHeight()/ 2)) + 15);
        
        batchMapa.end();
        // dibujando los layer de frente (los que 'tapan' al personaje)
        renderMapa.render( layersFrente );
        // actualizando escenario (gui)
        stage.act();
        // dibujando escenario (gui)
        stage.draw();
        
        batchMapa.enableBlending();
        batchMapa.begin();
        //batchMapa.draw(RecursosJuego.barb, 50, 50);
        batchMapa.draw(RecursosJuego.armorAmarilla.split(26, 40)[0][0], 50, 0);
        //batchMapa.draw(txtArbol, 0, 0);
        //batchMapa.draw(RecursosJuego.asdRegion, 100, 100);
        batchMapa.end();
	}

	/*
	 * Se liberan los recursos
	 */
	@Override
	public void dispose() {
		//renderMapa.dispose();
		fontFPS.dispose();
		stage.dispose();
		//RecursosJuego.liberarRecursos();
	}
	
		
	@Override
	public void resize(int width, int height) {
		camara.setToOrtho(false, (float)width, (float)height);
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void hide() {

	}

}
