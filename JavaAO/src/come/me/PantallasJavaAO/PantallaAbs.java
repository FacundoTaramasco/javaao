package come.me.PantallasJavaAO;

import com.badlogic.gdx.Screen;
import com.me.NucleoJavaAO.NucleoJavaAO;

public abstract class PantallaAbs  implements Screen {

	protected NucleoJavaAO juego;
	
	
	public PantallaAbs(NucleoJavaAO juego) {
		this.juego = juego;
	}
	
}
