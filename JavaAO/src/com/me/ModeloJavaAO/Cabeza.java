package com.me.ModeloJavaAO;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Array;
import com.me.NucleoJavaAO.RecursosJuego;

/**
 * Clase que contiene las texturas de la cabeza
 * @author Spirok
 * 06-06-15
 */
public class Cabeza {

	protected TextureRegion frameActualCabeza;

	protected Array<AtlasRegion> cabezaRegion;
	
	
	public Cabeza() {
		// obteniendo graficos de cabeza
		cabezaRegion = RecursosJuego.atlasCabezas.findRegions("cabeza");

		// estableciendo al primer frame de cabeza y de armadura
		frameActualCabeza  = cabezaRegion.get(0);
	}
}
