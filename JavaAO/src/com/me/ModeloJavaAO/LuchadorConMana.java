package com.me.ModeloJavaAO;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.me.NucleoJavaAO.NucleoJavaAO;
import com.me.UtilidadesJavaAO.ControladorVirtualPersonaje;

/**
 * Clase abstracta que representa un personaje luchador con mana
 * (mago, elfo, paladin, etc)
 * @author Spirok
 *
 */
public abstract class LuchadorConMana extends Luchador {

	private float mana;
	
	public LuchadorConMana(NucleoJavaAO juego, ControladorVirtualPersonaje ctrlV) {
		super(juego, ctrlV);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Metodo que mueve el personsaje si no se detecta una colision.
	 */
	@Override
	public void mover(float x, float y) {
		super.mover(x, y);
	}
	
	/**
	 * Metodo que actualiza el estado del personaje
	 */
	@Override
	public void actualizar(float delta) {	
		super.actualizar(delta);
	}
	
	/**
	 * Metodo que dibuja en patalla al personaje
	 */
	@Override
	public void dibujar(SpriteBatch sBatch) {	
		super.dibujar(sBatch);
	}	
	
	public void dispose() {
		super.dispose();
		
	}

	public float getMana() {
		return mana;
	}

	public void setMana(float mana) {
		this.mana = mana;
	}
	
}
