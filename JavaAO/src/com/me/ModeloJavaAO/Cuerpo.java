package com.me.ModeloJavaAO;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.me.NucleoJavaAO.RecursosJuego;

/**
 * Clase que contiene las animaciones del cuerpo de un IndividuoAbs.
 * @author spirok
 * 06-06-15
 */
public class Cuerpo {

	protected Animation     animacionAbajo; 
	protected Animation     animacionArriba;
	protected Animation     animacionIzquierda;
	protected Animation     animacionDerecha;

	protected TextureRegion primeroAbajo;
	protected TextureRegion primeroArriba;
	protected TextureRegion primeroIzquierda;
	protected TextureRegion primeroDerecha;

	protected TextureRegion frameQuieto;
	protected TextureRegion frameActual;

	public Cuerpo() {

		// obteniendo animaciones de vestimenta
		animacionAbajo     = new Animation(0.050f, RecursosJuego.armorAmarilla.split(25, 45)[0]);
		//animacionAbajo     = new Animation(0.050f, RecursosJuego.atlasArmadurasBarbaro.findRegions("abajo"));
		//animacionArriba    = new Animation(0.050f, RecursosJuego.atlasArmadurasBarbaro.findRegions("arriba"));
		//animacionIzquierda = new Animation(0.050f, RecursosJuego.atlasArmadurasBarbaro.findRegions("izquierda"));
		//animacionDerecha   = new Animation(0.050f, RecursosJuego.atlasArmadurasBarbaro.findRegions("derecha"));
	
		animacionArriba    = new Animation(0.050f, RecursosJuego.armorAmarilla.split(25, 45)[1]);
		animacionIzquierda = new Animation(0.050f, RecursosJuego.armorAmarilla.split(25, 45)[2]);
		animacionDerecha   = new Animation(0.050f, RecursosJuego.armorAmarilla.split(25, 45)[3]);
		//frameActual = RecursosJuego.atlasArmadurasBarbaro.findRegion("abajo");
		
		frameActual = RecursosJuego.armorAmarilla.split(25, 45)[0][0];
		frameQuieto = frameActual;
		
		// obteniendo el primer frame de vestimenta, dependiendo de la direccion
		primeroAbajo     = frameActual;
		//primeroArriba    = RecursosJuego.atlasArmadurasBarbaro.findRegion("arriba");
		//primeroIzquierda = RecursosJuego.atlasArmadurasBarbaro.findRegion("izquierda");
		//primeroDerecha   = RecursosJuego.atlasArmadurasBarbaro.findRegion("derecha");
		
		primeroArriba    = RecursosJuego.armorAmarilla.split(25, 45)[1][0];
		primeroIzquierda = RecursosJuego.armorAmarilla.split(25, 45)[2][0];
		primeroDerecha   = RecursosJuego.armorAmarilla.split(25, 45)[3][0];
	}	
	

	
	
}
