package com.me.ModeloJavaAO;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Rectangle;
import com.me.NucleoJavaAO.ManejadorAudio;
import com.me.NucleoJavaAO.NucleoJavaAO;
import com.me.UtilidadesJavaAO.ControladorVirtualPersonaje;
import com.me.UtilidadesJavaAO.ControladorVirtualPersonaje.ESTADOS;

/**
 * Clase abstracta que representa un personaje. Hereda de individuo
 * @author Spirok
 * 06-06-15
 */
public abstract class Personaje extends IndividuoAbs {

	private int energia;
	private BitmapFont fontNombre;
	
	private Cuerpo cuerpo;
	private Cabeza cabeza;
	
	private float stateTime;
	private float tiempo;

	private ControladorVirtualPersonaje ctrl;
	
	private ESTADOS estadoAcual = ESTADOS.quieto;

	private ShapeRenderer sr;
	
	public Personaje(NucleoJavaAO juego, ControladorVirtualPersonaje ctrlV) {
		super(juego);
		this.ctrl = ctrlV;
		
		fontNombre = new BitmapFont();
		
		this.cuerpo = super.getCuerpo();
		this.cabeza = super.getCabeza();
		
		sr = new ShapeRenderer();
	}


	public BitmapFont getFontNombre() { return fontNombre; }

	public int getEnergia() { return energia; }
	
	@Override
	public void mover(float x, float y) {
		// obteniendo posicion actual
		float antX = this.getX();
	    float antY = this.getY();
	    // moviendo a la nueva posicion
	    this.setPosition(getX() + x, getY() + y);
	    // si hay colision, mover a la posicion anterior
		for(MapObject ob : juego.getObjetosMapa()) {
			if (ob instanceof RectangleMapObject) {
		        Rectangle rect = ((RectangleMapObject) ob).getRectangle();
		        if (this.getRectangulo().overlaps(rect) ) { // colision
		        	this.setPosition(antX, antY);
		        	//while (!ctrl.pilaEstados.isEmpty()) ctrl.pilaEstados.remove();
		        	estadoAcual = ESTADOS.quieto;
		        }
			}
			/*
			if (ob instanceof PolylineMapObject ){
				ob = (MapObject) ob;
				float[] f = ((PolylineMapObject) ob).getPolyline().getVertices();
				for (Float fl : f) {
					System.out.println( fl);
				}
			}
			*/
		}
		
	}
	
	@Override
	public void actualizar(float delta) {
		// coleccion que contiene los estados del los movimientos del personaje,
		// se carga a medida que realiza un input en el dispositivo.
		if (!ctrl.pilaEstados.isEmpty() )
			estadoAcual = ctrl.pilaEstados.peek(); // obtener el ultimo valor
		else estadoAcual = ESTADOS.quieto;

		stateTime += delta;
		tiempo    += delta;

		if (estadoAcual == ESTADOS.arriba) {
			mover(0, getVelocidad()*delta );
		}
		if (estadoAcual == ESTADOS.abajo) {
			mover(0, -getVelocidad()*delta );
		}
		if (estadoAcual == ESTADOS.izquierda) {
			mover(-getVelocidad()*delta , 0);
		}
		if (estadoAcual == ESTADOS.derecha) {
			mover(getVelocidad()*delta , 0);
		}

		 if (estadoAcual != ESTADOS.quieto) {
			 // reproducir el sonido de caminar cada cierto tiempo
			 if ( tiempo >  (33.00f/getVelocidad()) ) {
	        	ManejadorAudio.reproducirSonido( ManejadorAudio.getSonido("pasos") );
	        	tiempo = 0f;
	        }
		 }
		// mover la camara en la posicion del personaje
		juego.getCamara().position.x = getX() + getWidth();
		juego.getCamara().position.y = getY() + getHeight();
		
	}
	
	@Override
	public void dibujar(SpriteBatch sb) {
        if (estadoAcual == ESTADOS.quieto) {
        	cuerpo.frameActual = cuerpo.frameQuieto;
        }
        else if (estadoAcual == ESTADOS.abajo) {        	
        	cabeza.frameActualCabeza = cabeza.cabezaRegion.get(0);
        	cuerpo.frameActual = cuerpo.animacionAbajo.getKeyFrame(stateTime, true);
        	cuerpo.frameQuieto = cuerpo.primeroAbajo;
        }
        else if (estadoAcual == ESTADOS.arriba) {
        	cabeza.frameActualCabeza = cabeza.cabezaRegion.get(1);
        	cuerpo.frameActual = cuerpo.animacionArriba.getKeyFrame(stateTime, true);
        	cuerpo.frameQuieto = cuerpo.primeroArriba;
        }
        else if (estadoAcual == ESTADOS.izquierda) {
        	cabeza.frameActualCabeza = cabeza.cabezaRegion.get(3);
        	cuerpo.frameActual = cuerpo.animacionIzquierda.getKeyFrame(stateTime, true);
        	cuerpo.frameQuieto = cuerpo.primeroIzquierda;
        }
        else if (estadoAcual == ControladorVirtualPersonaje.ESTADOS.derecha) {
        	cabeza.frameActualCabeza = cabeza.cabezaRegion.get(2);
        	cuerpo.frameActual = cuerpo.animacionDerecha.getKeyFrame(stateTime, true);
        	cuerpo.frameQuieto = cuerpo.primeroDerecha;
        }
        // dibujar en pantalla el frame correspondiente
        sb.draw(cabeza.frameActualCabeza, getX() + 5f  , getY() + 38f );
        sb.draw(cuerpo.frameActual,       getX() - 1.8f, getY()       );
        
        // dibujando nombre del pj
        getFontNombre().draw(sb, this.getNombre(), 
        		(this.getX() ) ,
        		(this.getY() ) );
        
        sb.end();
        sr.setProjectionMatrix(juego.getCamara().combined); 
        sr.begin(ShapeType.Line);
        //sr.rect(getX(), getY(), getWidth(), getHeight());
        sr.end();
        sb.begin();
	}
	
	public void dispose() {
		getFontNombre().dispose();
	}
	
}
