package com.me.ModeloJavaAO;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.me.NucleoJavaAO.NucleoJavaAO;
import com.me.UtilidadesJavaAO.ControladorVirtualPersonaje;

public class Paladin extends LuchadorConMana {

	public Paladin(NucleoJavaAO juego, ControladorVirtualPersonaje ctrlV) {
		super(juego, ctrlV);
		setMana(200);
		//setVelocidad(200);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Metodo que mueve el personsaje si no se detecta una colision.
	 */
	@Override
	public void mover(float x, float y) {
		super.mover(x, y);
	}
	
	/**
	 * Metodo que actualiza el estado del personaje
	 */
	@Override
	public void actualizar(float delta) {
		super.actualizar(delta);
	}
	
	/**
	 * Metodo que dibuja en patalla al personaje
	 */
	@Override
	public void dibujar(SpriteBatch sBatch) {	
		super.dibujar(sBatch);
	}	
	
	public void dispose() {
		super.dispose();
		
	}
	
}
