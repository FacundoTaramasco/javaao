package com.me.ModeloJavaAO;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.me.NucleoJavaAO.NucleoJavaAO;
import com.me.UtilidadesJavaAO.ControladorVirtualPersonaje;

/**
 * 
 * @author Spirok
 * 06/06/15
 */
public abstract class Luchador extends Personaje {

	/**
	 * Constructor
	 * @param juego
	 * @param ctrlV
	 */
	public Luchador(NucleoJavaAO juego, ControladorVirtualPersonaje ctrlV) {
		super(juego, ctrlV);
	}

	/**
	 * Metodo que mueve el personsaje si no se detecta una colision.
	 */
	@Override
	public void mover(float x, float y) {
		super.mover(x, y);
	}
	
	/**
	 * Metodo que actualiza el estado del personaje
	 */
	@Override
	public void actualizar(float delta) {	
		super.actualizar(delta);
	}
	
	/**
	 * Metodo que dibuja en patalla al personaje
	 */
	@Override
	public void dibujar(SpriteBatch sBatch) {	
		super.dibujar(sBatch);
	}	
	
	public void dispose() {
		super.dispose();
		
	}
}
