package com.me.ModeloJavaAO;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.me.NucleoJavaAO.NucleoJavaAO;

/**
 * Clase abstracta que representa un individuo del juego(un personaje, un 
 * ncp, etc)
 * @author Spirok
 * 06-06-15
 */
public abstract class IndividuoAbs extends Actor {

	protected NucleoJavaAO juego;
	
	private String nombre;
	private int vida;
	private float velocidad = 200.0f;
	
	private Cuerpo cuerpoIndividuo;
	private Cabeza cabezaIndividuo;
	
	public IndividuoAbs(NucleoJavaAO juego) {
		this.juego = juego;
		this.cuerpoIndividuo = new Cuerpo();
		this.cabezaIndividuo = new Cabeza();
		
		// importante settear ancho y alto del personaje, para el manejo de colisiones
		setWidth(getCuerpo().frameQuieto.getRegionWidth());
		setHeight(getCuerpo().frameQuieto.getRegionHeight());
	}
	

	// metodos abstractos
	public abstract void mover(float x, float y);

	public abstract void actualizar(float delta);
	
	public abstract void dibujar(SpriteBatch sb);

	// getters
	public float getVelocidad() { return this.velocidad; }

	public int getVida() { return vida; }
	
	public Rectangle getRectangulo() {  return new Rectangle(getX(), getY(), getWidth(), getHeight()); }
	
	public Cuerpo getCuerpo() { return cuerpoIndividuo; }
	
	public Cabeza getCabeza() { return cabezaIndividuo; }
	
	public String getNombre() { return nombre; }

	// Setters
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public void setVelocidad(float v) { velocidad = v; } 
}
