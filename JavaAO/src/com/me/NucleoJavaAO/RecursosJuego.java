package com.me.NucleoJavaAO;

import java.awt.image.BufferedImage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * Clase que carga todos los recursos del juego (sonidos, texturas, etc)
 * @author Spirok
 * 02/06/15
 */
public class RecursosJuego {

	public static AssetManager assets = new AssetManager();

	public static Skin skinApp = new Skin();
	
	public static TextureAtlas atlasArmadurasBarbaro;
	public static TextureAtlas atlasCabezas;

	public static TextureRegion armorAmarilla;
	
	
	public static void cargarRecursos() {
		assets.load(Constantes.rutaImagenes + "humanoColorado.pack", TextureAtlas.class);
		assets.load(Constantes.rutaImagenes + "armaduraBarbaro.pack",TextureAtlas.class);
		
		assets.load("wav/24.wav", Sound.class);	
		assets.load("wav/101.mp3", Music.class);

		//assets.update();
		
		assets.finishLoading();
		
		atlasCabezas          = assets.get(Constantes.rutaImagenes + "humanoColorado.pack");
		atlasArmadurasBarbaro = assets.get(Constantes.rutaImagenes + "armaduraBarbaro.pack");

		ManejadorAudio.agregarSonido("pasos", assets.get("wav/24.wav", Sound.class));
		ManejadorAudio.agregarMusica("introAO", assets.get("wav/101.mp3", Music.class));
		

		//armorAmarilla = acomodarImagen(Gdx.files.local(Constantes.rutaImagenes + "17.png"));
		
		armorAmarilla = new TextureRegion(new Texture(Gdx.files.local(Constantes.rutaImagenes + "17.png")));
	}
	
	
	public static void liberarRecursos() {
		assets.dispose();
	}
	
	public static TextureRegion acomodarImagen(FileHandle fileHandle) {
		Pixmap pixBarb;
		Pixmap pixBarb2;
		pixBarb = new Pixmap(fileHandle);
		pixBarb2 = new Pixmap(pixBarb.getWidth(), pixBarb.getHeight(), Pixmap.Format.RGBA8888);
		//pixBarb2.setBlending(Pixmap.Blending.None);
		//pixBarb2.setColor(new Color(0f, 0f, 0f, 1f));
		for (int x = 0 ; x < pixBarb.getWidth(); x++) {
			for (int y = 0 ; y < pixBarb.getHeight() ; y++) {
				//System.out.println(pixBarb.getPixel(x, y));
				if ((pixBarb.getPixel(x, y) != 255) && (pixBarb.getPixel(x, y) != -16759553)) {
					pixBarb2.setColor(pixBarb.getPixel(x, y));
					pixBarb2.fillRectangle(x, y, 1, 1);
				}
			}	
		}
		return new TextureRegion( new Texture(pixBarb2) );
	}
	
	

	
	
	
}
