package com.me.NucleoJavaAO;

public class Constantes {

	public static final String nombreJuego = "JavaAO";
	
	public static final String versionJuego = "0.1 alpha";
	
	public static final String rutaImagenes = "img/";
	
	public static final String rutaMapas = "mapas/";
	
	public static final String rutaSonidos = "wav/";
	
	public static final String objetoColisionMapa = "recBaldosa";
	
	public static int ANCHOPANTALLA = 800;
	
	public static int ALTOPANTALLA = 600;
}
