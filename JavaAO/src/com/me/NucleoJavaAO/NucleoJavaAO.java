package com.me.NucleoJavaAO;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.me.ModeloJavaAO.IndividuoAbs;
import com.me.ModeloJavaAO.Paladin;
import com.me.UtilidadesJavaAO.ControladorVirtualPersonaje;
import com.me.UtilidadesJavaAO.EntradaMousePersonaje;
import com.me.UtilidadesJavaAO.EntradaTecladoPersonaje;

import come.me.PantallasJavaAO.PantallaInicio;
import come.me.PantallasJavaAO.PantallaPrincipal;

/**
 * Clase principal del juego
 * encargada de las funciones principales del juego : creacion(inicio del juego),
 * dibujo del graficos (render), redimensionado de pantalla(resize), etc
 * @author Spirok
 * 02/06/15
 */
public class NucleoJavaAO extends Game {
	
	private Screen pInicio;
	private Screen pantallaPrincipal;

	private OrthographicCamera camara;
	private SpriteBatch batchMapa;
	
	private TiledMap mapa;
	private OrthogonalTiledMapRenderer renderMapa;
	private MapObjects objetosMapa;
	
	private IndividuoAbs pruebaPJ;
	
	private InputMultiplexer multiP;
	private EntradaTecladoPersonaje inputTeclado;
	private EntradaMousePersonaje inputMousePersonaje;
	
	private String nombreClase = NucleoJavaAO.class.getName();
	
	public TiledMap getMapa() { return mapa; }
	
	// se llama al crear la clase
	@Override
	public void create() {
		init();
	}
	
	
	private void init() {
		// cargando recursos del juego(graficos, sonidos, etc)
		RecursosJuego.cargarRecursos();

		// creando mapa, obteniendo su render, objetos, y batch
		mapa        = new TmxMapLoader().load(Constantes.rutaMapas + "mapa1.tmx");
		renderMapa  = new OrthogonalTiledMapRenderer(mapa);
		objetosMapa = mapa.getLayers().get(Constantes.objetoColisionMapa).getObjects();
		batchMapa   = (SpriteBatch) renderMapa.getBatch();
		
		//renderMapa.renderObjects( mapa.getLayers().get("piso") );
		//renderMapa.renderObjects( mapa.getLayers().get("casa") );
		//renderMapa.renderObjects( mapa.getLayers().get("asd") );
		
		for (MapLayer mp : mapa.getLayers() ) {
			System.out.println(mp.getName());
			
			for (MapObject mo : mp.getObjects()) {
				System.out.println(mo.getClass());
			}
			
		}
		
		
		// creando camara
		camara = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight() );

		
		Preferencias.setReproduceSonido(true);
		Preferencias.setReproduceMusica(true);
		Preferencias.setearVolumenMusica(1.0f);
		Preferencias.setearVolumenSonido(1.0f);
		
		// debug, mostrando preferencias del juego
		//Preferencias.mostrarPreferencias();

		//
		ControladorVirtualPersonaje ctrl = new ControladorVirtualPersonaje();
		
		multiP              = new InputMultiplexer();
		inputTeclado        = new EntradaTecladoPersonaje(ctrl);
		inputMousePersonaje = new EntradaMousePersonaje(ctrl);
		multiP.addProcessor(inputTeclado);
		multiP.addProcessor(inputMousePersonaje);
		multiP.addProcessor(new InputAdapter() {	
			@Override
			public boolean keyTyped(char character) {
				if (character == ' ') init();
				return false;
			}
		});
		Gdx.input.setInputProcessor(multiP);
		
		// creando personaje
		pruebaPJ   = new Paladin(this, ctrl);
		pruebaPJ.mover(300, 800 );
		
		// posicionando la camara sobre el personaje
		camara.position.x = pruebaPJ.getX() + pruebaPJ.getWidth();
		camara.position.y = pruebaPJ.getY() + pruebaPJ.getHeight();
		
		// creando pantalla principal del juego y seteando screen
		pantallaPrincipal = new PantallaPrincipal(this);
		pInicio = new PantallaInicio(this);
		
		this.setScreen(pInicio);
		

	}
	
	/*
	 public void actualizar(float delta) {

	 }
	 */
	
	public IndividuoAbs getPersonajePrueba() { return this.pruebaPJ; }
	
	public SpriteBatch getSBMapa() { return this.batchMapa; }
	
	public OrthographicCamera getCamara() { return this.camara; }
	
	public OrthogonalTiledMapRenderer getRenderMapa() { return this.renderMapa; }

	public MapObjects getObjetosMapa() { return objetosMapa; }
	
	public Screen getPantallaPrincipal() { return pantallaPrincipal; }
	
	public Screen getPantallaInicio() { return pInicio; }
	
	public InputMultiplexer getMulti() { return multiP; }
	
	@Override
	public void dispose() {
		Gdx.app.log(nombreClase, "liberando recursos");
		//getPersonajePrueba().dispose();
		RecursosJuego.liberarRecursos();
	}
}
