package com.me.NucleoJavaAO;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

/**
 * Clase que maneja las preferencias del juego.
 * Por defecto el archivo se guarda en :
 * 
 * Windows	%UserProfile%/.prefs/My Preferences
 * Linux and OS X	~/.prefs/My Preferences
 * @author spirok
 * 02/06/15
 *
 */
public class Preferencias {

	private static final Preferences prefs = Gdx.app.getPreferences("My Preferences");
	
	public static void mostrarPreferencias() {
		System.out.println(
				"reproduceSonido : " + getReproduceSonido() + "\n" +
				"reproduceMusica : " + getReproduceMusica() + "\n" +
				"volumenSonido : " + getVolmenSonido() + "\n" +
				"volumenMusica : " + getVolumenMusica()
				);
	}
	
	public static boolean getReproduceSonido() { return prefs.getBoolean("reproduceSonido"); }
	
	public static boolean getReproduceMusica() { return prefs.getBoolean("reproduceMusica"); }
	
	public static float getVolmenSonido() { return prefs.getFloat("volumenSonido"); }
	
	public static float getVolumenMusica() { return prefs.getFloat("volumenMusica"); }
	
	public static void setearVolumenSonido(float vol) {
		prefs.putFloat("volumenSonido", vol);
		prefs.flush();
	}

	public static void setearVolumenMusica(float vol) {
		prefs.putFloat("volumenMusica", vol);
		prefs.flush();
	}
	
	public static void setReproduceSonido(boolean b) {
		prefs.putBoolean("reproduceSonido", b); 
		prefs.flush();
		}
	
	public static void setReproduceMusica(boolean b) {
		prefs.putBoolean("reproduceMusica", b);
		prefs.flush();
		}
	
}
