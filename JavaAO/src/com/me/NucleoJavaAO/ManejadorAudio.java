package com.me.NucleoJavaAO;


import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

/**
 * Clase que gestiona la musica y sonidos del juego.
 * @author spirok
 * 02/06/15
 */
public class ManejadorAudio {

	public ManejadorAudio() {
	}
	
	// mapa cargado en RecursosJuego
	private static HashMap<String, Sound> mapaSonidos = new HashMap<String, Sound>();
	
	private static HashMap<String, Music> mapaMusicas = new HashMap<String, Music>();

	// getters
	private static HashMap<String, Sound> getMapaSonidos() { return mapaSonidos; }
	
	private static HashMap<String, Music> getMapaMusicas() { return mapaMusicas; }
	
	public static Sound getSonido(String nombre) { return getMapaSonidos().get(nombre); }
	
	public static Music getMusica(String nombre) { return getMapaMusicas().get(nombre); }

	
	public static void reproducirSonido(Sound sonido) {
		if (Preferencias.getReproduceSonido())
			sonido.play(Preferencias.getVolmenSonido());
	}
	
	public static void reproducirMusica(Music musica) {
		if (Preferencias.getReproduceMusica())
			musica.play();
	}

	/*
	 * Se agrega un sonido al mapa
	 */
	public static void agregarSonido(String nombre, Sound sonido) {
		getMapaSonidos().put(nombre, sonido);
	}
	
	/*
	 * Se agrega musica al mapa
	 */
	public static void agregarMusica(String nombre, Music musica) {
		getMapaMusicas().put(nombre, musica);
	}
	
	public static void habilitarSonidos() {
		/*for (String key : getMapaSonidos().keySet()) {
			getMapaSonidos().get(key).resume();
		}*/
		Preferencias.setReproduceSonido(true);
	}
	
	public static void silenciarSonidos() {
		for (String key : getMapaSonidos().keySet()) {
			getMapaSonidos().get(key).pause();
		}
		Preferencias.setReproduceSonido(false);
	}
	
	public static void habilitarMusica() {
		for (String key : getMapaMusicas().keySet()) {
			getMapaMusicas().get(key).setVolume(Preferencias.getVolumenMusica());
		}
		Preferencias.setReproduceMusica(true);
	}
	
	public static void silenciarMusica() {
		for (String key : getMapaMusicas().keySet()) {
			getMapaMusicas().get(key).setVolume(0f);
		}
		Preferencias.setReproduceMusica(false);
	}
	
	
	
	
	
	
	/*
	 * VERIFICAR ESTO..............
	 */
	public static void setearVolumenMusica(float vol) {
		for (String key : getMapaMusicas().keySet() ) {
			getMapaMusicas().get(key).setVolume(vol);
		}
		Preferencias.setearVolumenMusica(vol);
	}
	
	public static void setearVolumenSonido(float vol) {
		Preferencias.setearVolumenSonido(vol);
	}
}
