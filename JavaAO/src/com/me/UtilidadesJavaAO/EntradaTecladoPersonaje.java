package com.me.UtilidadesJavaAO;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.me.UtilidadesJavaAO.ControladorVirtualPersonaje.ESTADOS;

/**
 * Clase que se encarga de la entrada de teclado del personaje.
 * @author spirok
 *
 */
public class EntradaTecladoPersonaje extends InputAdapter {

	private ControladorVirtualPersonaje ctrl;
	
	private int teclaArriba = Input.Keys.UP;
	private int teclaAbajo  = Input.Keys.DOWN;
	private int teclaIzq    = Input.Keys.LEFT;
	private int teclaDer    = Input.Keys.RIGHT;
	
	public EntradaTecladoPersonaje(ControladorVirtualPersonaje ctrl) {
		this.ctrl = ctrl;
	}
	
	@Override
	public boolean keyDown(int keycode) {
		if ((keycode == teclaArriba) ) {
			ctrl.pilaEstados.push(ESTADOS.arriba);
			return true;
		}
		if ((keycode == teclaAbajo) ) {
			ctrl.pilaEstados.push(ESTADOS.abajo);
			return true;
		}
		if (keycode == teclaIzq) {
			ctrl.pilaEstados.push(ESTADOS.izquierda);
			return true;
		}
		if (keycode == teclaDer) {
			ctrl.pilaEstados.push(ESTADOS.derecha);
			return true;
		}
		 return false;
	}
	
	@Override
	public boolean keyUp(int keycode) {
		if (keycode == teclaArriba) {
			ctrl.pilaEstados.remove(ESTADOS.arriba);
			return true;
		}
		if (keycode == teclaAbajo) {
			ctrl.pilaEstados.remove(ESTADOS.abajo);
			return true;
		}
		if (keycode == teclaIzq) {
			ctrl.pilaEstados.remove(ESTADOS.izquierda);
			return true;
		}
		if (keycode == teclaDer) {
			ctrl.pilaEstados.remove(ESTADOS.derecha);
			return true;
		}
		return false;
	}
	
	
	
}
