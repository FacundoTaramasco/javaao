package com.me.UtilidadesJavaAO;

import java.util.ArrayDeque;
import java.util.Deque;
/**
 * Clase que contiene atributos referenciando a que lado se mueve el personaje.
 * @author spirok
 */
public class ControladorVirtualPersonaje {
	
	public enum ESTADOS {
		arriba,
		abajo,
		izquierda,
		derecha,
		quieto
	}
	
	public Deque<ESTADOS> pilaEstados = new ArrayDeque<ESTADOS>();
	
}
