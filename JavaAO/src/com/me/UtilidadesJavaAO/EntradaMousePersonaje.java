package com.me.UtilidadesJavaAO;

import com.badlogic.gdx.InputAdapter;

public class EntradaMousePersonaje extends InputAdapter {

	private ControladorVirtualPersonaje ctrl;
	
	public EntradaMousePersonaje(ControladorVirtualPersonaje ctrl) {
		this.ctrl = ctrl;
	}
	
	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		//System.out.println(screenX + " " + screenY);
		return true;
	}
}
