package com.me.JavaAO;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.me.NucleoJavaAO.Constantes;
import com.me.NucleoJavaAO.NucleoJavaAO;

/**
 * 
 * @author spirok
 * 02/06/15
 */
public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = Constantes.nombreJuego + " " + Constantes.versionJuego;
		cfg.width = Constantes.ANCHOPANTALLA;
		cfg.height = Constantes.ALTOPANTALLA;
		cfg.resizable = true;
		new LwjglApplication(new NucleoJavaAO(), cfg);
	}
}
