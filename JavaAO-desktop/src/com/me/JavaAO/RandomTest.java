package com.me.JavaAO;

import java.util.Iterator;
import java.util.Stack;

public class RandomTest {

	
	public static void main(String[] args) {
		
		Stack<String> pila = new Stack<String>();
		
		pila.push(new String("asd"));
		pila.push(new String("2"));
		
		
		Iterator<String> iterator = pila.listIterator();

		while(iterator.hasNext()) {
			System.out.println(iterator.next());
		}
		
		System.out.println(pila.peek());
		/*
		pila.pop();
		pila.pop();
		iterator = pila.listIterator();
		while(iterator.hasNext()) {
			System.out.println(iterator.next());
		}
		*/
		
	}
}
